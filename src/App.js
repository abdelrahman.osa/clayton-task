import React, { useState, useEffect } from 'react';
import { AxiosGet, AxiosPost } from './Axios/Axios';
import { Pagination } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import TextField from "@mui/material/TextField";
import classes from './App.module.css';


function App() {

  const [listData, setListData] = useState(null)
  const [pageNum, setPageNum] = useState(0)
  const [searchVal, setSearchVal] = useState(null)

  useEffect(()=>{
    AxiosGet(setListData);  
 },[])


 const searchFunChange = (e) => {
  setSearchVal(e.target.value)
 }

 const searchFunPress = (e) => {
  if(e.key === "Enter" || e.type === "click")
    {
      AxiosPost(searchVal, setListData)
    }
   
 }

 const paginationChange = (e,page) =>{
  setPageNum(page - 1)
 }

  return (
    <div className="App">
      <h1 className={classes.Title}>Star Trek: American Science Fiction Media</h1>
      {listData === null  ? 
      // Loader
      <div className={classes.Loader}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>

    : <div>
      {/* Table list Data */}
     <table className={classes.table}>
     <thead>
        <tr>
          <th className={classes.leftSideSty}>Name</th>
          <th className={classes.middleSideSty}>Status</th>
          <th className={classes.middleSideSty}>Spacecraft Class</th>
          <th className={classes.middleSideSty}>Registry</th>
          <th className={classes.rightSideSty}>Date Status</th>
          <th>
          <TextField
          id="search-bar"
          onChange={(e)=> searchFunChange(e)}
          onKeyDown={(e) => searchFunPress(e)}
          InputProps={{
            startAdornment: (
              <IconButton type="submit" aria-label="search" onClick={(e)=> searchFunPress(e)} >
              <SearchIcon style={{ fill: "blue" }} />
            </IconButton>
            ),
          }}
          variant="outlined"
          placeholder="Search..."
          size="small"
    />
          </th>
        </tr>
      </thead>
      <tbody>
        {(listData != null && listData.length != 0)?
          listData.slice((pageNum * 10) , (pageNum * 10 + 10)).map(Item => (
            <tr key={Item.uid}>
              <td className={classes.leftSideSty}>{Item.name? Item.name : '...'}</td>
              <td className={classes.middleSideSty}>{Item.status? Item.status : '...'}</td>
              <td className={classes.middleSideSty}>{Item.spacecraftClass && Item.spacecraftClass.name ? Item.spacecraftClass.name : '...'}</td>
              <td className={classes.middleSideSty}>{Item.registry? Item.registry : '...'}</td>
              <td className={classes.rightSideSty}>{Item.dateStatus? Item.dateStatus : '...'}</td>
              <td></td>
            </tr>
          ))
        :<tr>
          <td colSpan="6">Searched for the phrase and There were 0 result</td>
        </tr>}
      </tbody>
      <tfoot>
        <tr>
          <td colSpan="6">
            {/* Pagination */}
             <Pagination 
             count={(listData != null && listData.length != 0)? (Math.ceil(listData.length / 10)) : 0} onChange={paginationChange}
             className={classes.PaginationSty}
             color="primary"
             size="large" />
          </td>
        </tr>
      </tfoot>
     </table>
     </div>}
    </div>
  );
}

export default App;
