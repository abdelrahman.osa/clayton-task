
import axios from 'axios';

export function AxiosGet (setListData)  {
   
       axios.get('http://stapi.co/api/v2/rest/spacecraft/search')
              .then((response)=>{
                  setListData(response.data.spacecrafts)
              })    
      }


export function AxiosPost (searchVal, setListData) {

      if(searchVal != null)
      {
            let formBody = [];
            let bodyKey = 'name'
            let bodyValue = searchVal

            formBody.push(bodyKey + "=" + bodyValue);
            formBody = formBody.join("&");

            axios.post('http://stapi.co/api/v2/rest/spacecraft/search', formBody, {
                  headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                  }})
                  .then((response)=>{
                        setListData(response.data.spacecrafts)
                  }) 
      }
}